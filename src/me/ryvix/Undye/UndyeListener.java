/**
 * Undye - Undye wool by using a cauldron.
 * Copyright (C) 2012-2015 Ryan Rhode - rrhode@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package me.ryvix.Undye;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class UndyeListener implements Listener {
	
	public UndyeListener(Undye plugin) {
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerInteract(PlayerInteractEvent event) {
		if (event.getPlayer().getItemInHand().getType() != Material.WOOL || event.getAction() != Action.RIGHT_CLICK_BLOCK) {
			return;
		}
		
		Block block = event.getClickedBlock();
		
		if (block.getType() == Material.CAULDRON) {
			byte waterLevel = block.getData();
			
			if (waterLevel > 0) {
				int size = event.getPlayer().getItemInHand().getAmount();
				int level = waterLevel - size;
				if (level < 0) {
					level = 0;
				}
				block.setData((byte) level);
				event.getPlayer().setItemInHand(new ItemStack(Material.WOOL, size));
				event.setCancelled(true);
			}
		}
	}
}
