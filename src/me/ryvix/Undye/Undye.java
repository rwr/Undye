/**
 * Undye - Undye wool by using a cauldron.
 * Copyright (C) 2012-2015 Ryan Rhode - rrhode@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package me.ryvix.Undye;

import org.bukkit.plugin.java.JavaPlugin;

public class Undye extends JavaPlugin {

	@Override
	public void onEnable() {
		UndyeListener undyeListener = new UndyeListener(this);
		getServer().getPluginManager().registerEvents(undyeListener, this);
	}

	@Override
	public void onDisable() {
	}
}
